﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWatcher.Core.Extensions;
using Xunit;

namespace NetWatcher.Core.Tests
{
    public class StringExtensionsTests
    {
        [Theory]
        [InlineData("05", new byte[] {5})]
        [InlineData("0506", new byte[] { 5, 6 })]
        [InlineData("506", new byte[] { 5, 6 })]
        [InlineData("A5", new byte[] { 0xa5 })]
        public void FromHexPairsToBytes_CanConvert(string input, byte[] expected)
        {
            Assert.Equal(expected, input.FromHexPairsToBytes());
        }

        [Theory]
        [InlineData("0x05")]
        public void FromHexPairsToBytes_ThrowsOnInvalid(string input)
        {
            Assert.Throws<FormatException>(() => input.FromHexPairsToBytes().ToArray());
        }

        [Theory]
        [InlineData("1", 1)]
        [InlineData("10", 10)]
        [InlineData(null, default(int))]
        [InlineData("", default(int))]
        [InlineData(" ", default(int))]
        [InlineData(" \t", default(int))]
        [InlineData("XYZ", default(int))]
        public void ToInt32_CanConvert(string input, int expected)
        {
            Assert.Equal(expected, input.ToInt32());
        }

        [Theory]
        [InlineData("1", 1)]
        [InlineData("10", 10)]
        [InlineData("1.5", 1.5)]
        [InlineData(null, default(int))]
        [InlineData("", default(int))]
        [InlineData(" ", default(int))]
        [InlineData(" \t", default(int))]
        [InlineData("XYZ", default(int))]
        public void ToDecimal_CanConvert(string input, decimal expected)
        {
            Assert.Equal(expected, input.ToDecimal());
        }
    }
}

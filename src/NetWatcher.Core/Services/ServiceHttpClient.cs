﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace NetWatcher.Core.Services
{
    public class ServiceHttpClient<T> : HttpClient
    {
        public ServiceHttpClient() : base()
        {
        }

        public ServiceHttpClient(HttpMessageHandler handler) : base(handler)
        {
        }

        public ServiceHttpClient(HttpMessageHandler handler, bool disposeHandler) : base(handler, disposeHandler)
        {
        }
    }
}

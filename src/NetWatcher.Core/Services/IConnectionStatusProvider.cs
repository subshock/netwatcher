﻿using NetWatcher.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Core.Services
{
    public interface IConnectionStatusProvider
    {
        string Name { get; }
        Task<ConnectionStatusInfo> GetConnectionStatus();
    }
}

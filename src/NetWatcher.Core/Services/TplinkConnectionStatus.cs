﻿using NetWatcher.Core.Configuration;
using NetWatcher.Core.Extensions;
using NetWatcher.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NetWatcher.Core.Services
{
    public class TplinkConnectionStatus : IConnectionStatusProvider, IDisposable
    {
        private readonly TplinkConfiguration _config;
        private readonly HttpClient _http;

        public TplinkConnectionStatus(TplinkConfiguration config, ServiceHttpClient<TplinkConnectionStatus> httpClient)
        {
            _config = config;
            _http = httpClient;
            _http.BaseAddress = new Uri(config.Url + "/");
            _http.DefaultRequestHeaders.Referrer = _http.BaseAddress;
        }

        public string Name => "TP-Link";

        public async Task<ConnectionStatusInfo> GetConnectionStatus()
        {
            var rsa = await GetCrypto();
            var username = EncryptString(rsa, _config.Username, false);
            var password = EncryptString(rsa, _config.Password, true);

            var sessionId = await GetSessionId(username, password);

            if (sessionId == null)
                return new ConnectionStatusInfo
                {
                    Success = false,
                    Error = "Login failed"
                };

            var tokenId = await GetTokenId();

            if (tokenId == null)
                return new ConnectionStatusInfo
                {
                    Success = false,
                    Error = "Could not get token"
                };

            return await GetStatus();
        }

        private async Task<ConnectionStatusInfo> GetStatus()
        {
            var sb = new StringBuilder();
            sb.AppendLine("[IGD_DEV_INFO#0,0,0,0,0,0#0,0,0,0,0,0]0,2");
            sb.AppendLine("hardwareVersion");
            sb.AppendLine("softwareVersion");
            sb.AppendLine("[WAN_IP_CONN#1,1,1,0,0,0#0,0,0,0,0,0]1,2");
            sb.AppendLine("connectionStatus");
            sb.AppendLine("externalIPAddress");
            sb.AppendLine("[WAN_DSL_INTF_CFG#1,0,0,0,0,0#0,0,0,0,0,0]2,13");
            sb.AppendLine("status");
            sb.AppendLine("modulationType");
            sb.AppendLine("X_TP_AdslModulationCfg");
            sb.AppendLine("upstreamCurrRate");
            sb.AppendLine("downstreamCurrRate");
            sb.AppendLine("X_TP_AnnexType");
            sb.AppendLine("upstreamMaxRate");
            sb.AppendLine("downstreamMaxRate");
            sb.AppendLine("upstreamNoiseMargin");
            sb.AppendLine("downstreamNoiseMargin");
            sb.AppendLine("upstreamAttenuation");
            sb.AppendLine("downstreamAttenuation");
            sb.AppendLine("X_TP_UpTime");
            sb.AppendLine("[WAN_DSL_INTF_STATS_TOTAL#1,0,0,0,0,0#0,0,0,0,0,0]3,8");
            sb.AppendLine("ATUCCRCErrors");
            sb.AppendLine("CRCErrors");
            sb.AppendLine("ATUCFECErrors");
            sb.AppendLine("FECErrors");
            sb.AppendLine("SeverelyErroredSecs");
            sb.AppendLine("X_TP_US_SeverelyErroredSecs");
            sb.AppendLine("erroredSecs");
            sb.AppendLine("X_TP_US_ErroredSecs");

            var requestContent = new StringContent(sb.ToString());
            var response = await _http.PostAsync("cgi?1&1&1&1", requestContent);

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Error: {response.StatusCode}");

            var data = await response.Content.ReadAsStringAsync();
            var rawInfo = ParseConnectionInfo(data);

            if (rawInfo.Count >= 4)
            {
                var ret = new ConnectionStatusInfo
                {
                    Success = true,
                    HardwareVersion = GetParam(rawInfo, 0, "hardwareVersion"),
                    SoftwareVersion = GetParam(rawInfo, 0, "softwareVersion"),

                    DslStatus = ConvertConnectionStatus(GetParam(rawInfo, 2, "status")),
                    DslDescription = $"{GetParam(rawInfo, 2, "modulationType")} - {GetParam(rawInfo, 2, "X_TP_AdslModulationCfg")} - {GetParam(rawInfo, 2, "X_TP_AnnexType")}",
                    DslUptime = new TimeSpan(0, 0, GetParam(rawInfo, 2, "X_TP_UpTime").ToInt32()),
                    CurrentRate = new ConnectionStatusInfo.BidiValue<int>(GetParam(rawInfo, 2, "upstreamCurrRate").ToInt32(), GetParam(rawInfo, 2, "downstreamCurrRate").ToInt32()),
                    MaxRate = new ConnectionStatusInfo.BidiValue<int>(GetParam(rawInfo, 2, "upstreamMaxRate").ToInt32(), GetParam(rawInfo, 2, "downstreamMaxRate").ToInt32()),
                    LineAttenuation = new ConnectionStatusInfo.BidiValue<decimal>(GetParam(rawInfo, 2, "upstreamAttenuation").ToDecimal() / 10M, GetParam(rawInfo, 2, "downstreamAttenuation").ToDecimal() / 10M),
                    SnrMargin = new ConnectionStatusInfo.BidiValue<decimal>(GetParam(rawInfo, 2, "upstreamNoiseMargin").ToDecimal() / 10M, GetParam(rawInfo, 2, "downstreamNoiseMargin").ToDecimal() / 10M),

                    ErrorPackets = new ConnectionStatusInfo.BidiValue<int>(GetParam(rawInfo, 3, "ATUCCRCErrors").ToInt32(), GetParam(rawInfo, 3, "CRCErrors").ToInt32()),

                    NetStatus = ConvertNetStatus(GetParam(rawInfo, 1, "connectionStatus")),
                    IpAddress = GetParam(rawInfo, 1, "externalIPAddress")
                };

                return ret;
            }

            return new ConnectionStatusInfo() { Success = false, Error = "No specific error" };
        }

        private DslStatusType ConvertConnectionStatus(string state)
        {
            switch (state)
            {
                case "Up": return DslStatusType.Connected;
                case "Initializing": return DslStatusType.Initializing;
                default: return DslStatusType.Disconnected;
            }
        }

        private NetStatusType ConvertNetStatus(string state)
        {
            switch (state)
            {
                case "Connected": return NetStatusType.Connected;
                default: return NetStatusType.Disconnected;
            }
        }

        private string GetParam(List<Dictionary<string, string>> list, int index, string key)
        {
            if (list.Count > index && list[index].ContainsKey(key))
                return list[index][key];

            return null;
        }

        private async Task<string> GetTokenId()
        {
            var response = await _http.GetAsync("/");

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Error: {response.StatusCode}");

            var data = await response.Content.ReadAsStringAsync();

            var regex = new Regex("var token=\"([^\"]+)\"", RegexOptions.Singleline);
            var match = regex.Match(data);

            if (match.Success && match.Groups.Count >= 2)
            {
                _http.DefaultRequestHeaders.Add("TokenId", match.Groups[1].Value);
                return match.Groups[1].Value;
            }

            return null;
        }

        private async Task<string> GetSessionId(string username, string password)
        {
            var response = await _http.PostAsync($"cgi/login?UserName={username}&Passwd={password}&Action=1&LoginStatus=0", null);

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Error: {response.StatusCode}");

            response.Headers.TryGetValues("Set-Cookie", out var cookies);

            var regex = new Regex("JSESSIONID=([^;]+);");

            foreach (var cookie in cookies)
            {
                var match = regex.Match(cookie);

                if (match.Success && match.Groups.Count >= 2)
                {
                    if (match.Groups[1].Value == "deleted") return null;

                    _http.DefaultRequestHeaders.Add("Cookie", $"JSESSIONID={match.Groups[1].Value}");
                    return match.Groups[1].Value;
                }
            }

            return null;
        }

        private async Task<RSA> GetCrypto()
        {
            var response = await _http.PostAsync("cgi/getParm", null);

            if (!response.IsSuccessStatusCode)
                throw new Exception($"Error: {response.StatusCode}");

            var text = await response.Content.ReadAsStringAsync();

            var regex = new Regex(".*var ee=\"(.+)\";.*var nn=\"(.+)\";", RegexOptions.Singleline);

            var matches = regex.Match(text);

            if (matches.Success && matches.Groups.Count >= 3)
            {
                var rsaParams = new RSAParameters
                {
                    Exponent = matches.Groups[1].Value.FromHexPairsToBytes().ToArray(),
                    Modulus = matches.Groups[2].Value.FromHexPairsToBytes().ToArray()
                };

                var rsa = RSA.Create();
                rsa.ImportParameters(rsaParams);

                return rsa;
            }

            throw new Exception("Couldn't retrieve login parameters");
        }

        private string EncryptString(RSA rsa, string text, bool base64Encode)
        {
            if (base64Encode)
                text = Convert.ToBase64String(Encoding.UTF8.GetBytes(text));

            var inputBytes = Encoding.UTF8.GetBytes(text);

            var outputBytes = rsa.Encrypt(inputBytes, RSAEncryptionPadding.Pkcs1);

            var sb = new StringBuilder();

            foreach (var b in outputBytes)
                sb.AppendFormat("{0:x2}", b);

            return sb.ToString();
        }

        private List<Dictionary<string, string>> ParseConnectionInfo(string data)
        {
            var list = new Dictionary<string, Dictionary<string, string>>();
            string currentDataSet = null;

            using (var rdr = new StringReader(data))
            {
                string line;
                while ((line = rdr.ReadLine()) != null)
                {
                    if (line.StartsWith("[")) {
                        list.Add(line, new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase));
                        currentDataSet = line;
                    }
                    else
                    {
                        var idx = line.IndexOf("=", StringComparison.Ordinal);

                        if (idx >= 0 && currentDataSet != null)
                        {
                            list[currentDataSet].Add(line.Substring(0, idx), line.Substring(idx + 1));
                        }
                    }
                }
            }

            return list.Values.ToList();
        }

        public void Dispose()
        {
            _http.Dispose();
        }
    }
}

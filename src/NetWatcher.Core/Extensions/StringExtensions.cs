﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetWatcher.Core.Extensions
{
    public static class StringExtensions
    {
        public static IEnumerable<byte> FromHexPairsToBytes(this string text)
        {
            if (text.Length % 2 == 1) text = "0" + text;

            var idx = 0;
            while (idx < text.Length)
            {
                yield return byte.Parse(text.Substring(idx, 2), System.Globalization.NumberStyles.HexNumber);

                idx += 2;
            }
        }

        public static int ToInt32(this string text)
        {
            if (string.IsNullOrWhiteSpace(text) || !int.TryParse(text, out var result)) return default(int);

            return result;
        }

        public static decimal ToDecimal(this string text)
        {
            if (string.IsNullOrWhiteSpace(text) || !decimal.TryParse(text, out var result)) return default(decimal);

            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Core.Models
{
    public enum DslStatusType
    {
        Disconnected,
        Initializing,
        Connected
    }

    public enum NetStatusType
    {
        Connected,
        Disconnected
    }

    public class ConnectionStatusInfo
    {
        public bool Success { get; set; }
        public string Error { get; set; }

        // Router Information
        public string HardwareVersion { get; set; }
        public string SoftwareVersion { get; set; }
        
        // DSL Status
        public DslStatusType DslStatus { get; set; }
        public string DslDescription { get; set; }
        public TimeSpan DslUptime { get; set; }
        public BidiValue<int> CurrentRate { get; set; }
        public BidiValue<int> MaxRate { get; set; }
        public BidiValue<decimal> SnrMargin { get; set; }
        public BidiValue<decimal> LineAttenuation { get; set; }
        public BidiValue<int> ErrorPackets { get; set; }

        // Internet Status
        public NetStatusType NetStatus { get; set; }
        public string IpAddress { get; set; }

        public struct BidiValue<T> where T: struct
        {
            public T Up { get; }
            public T Down { get; }

            public BidiValue(T up, T down)
            {
                Up = up;
                Down = down;
            }
        }
    }
}

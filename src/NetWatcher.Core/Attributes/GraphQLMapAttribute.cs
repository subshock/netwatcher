﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetWatcher.Core.Attributes
{
    public class GraphQLMapAttribute : Attribute
    {
        public string GraphQLProperty { get; set; }

        public GraphQLMapAttribute(string graphQLProperty)
        {
            GraphQLProperty = graphQLProperty;
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Core.Configuration
{
    public abstract class ConfigurationBase
    {
        protected abstract string Section { get; }

        private readonly IConfiguration _config;
        private IChangeToken _token;

        protected ConfigurationBase(IConfiguration config)
        {
            _config = config;

            if (_config != null)
                ReloadConfiguration(null);
        }

        private void ReloadConfiguration(object state)
        {
            _config.GetSection(Section)?.Bind(this);
            _token = _config.GetReloadToken();
            _token.RegisterChangeCallback(ReloadConfiguration, null);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace NetWatcher.Core.Configuration
{
    public class TplinkConfiguration : ConfigurationBase
    {
        protected override string Section => "TpLink";

        public string Url { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public TplinkConfiguration(IConfiguration config) : base(config)
        {
        }
    }
}

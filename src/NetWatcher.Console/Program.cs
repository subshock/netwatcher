﻿using NetWatcher.Core.Configuration;
using NetWatcher.Core.Services;
using System;
using System.Net.Http;
using Con = System.Console;

namespace NetWatcher.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new TplinkConfiguration(null)
            {
                Url = "http://192.168.1.254",
                Username = "admin",
                Password = "V6Zy47nf"
            };

            var svc = new TplinkConnectionStatus(config);

            Con.WriteLine("NetWatcher.Console");
            Con.WriteLine(new string('-', Con.WindowWidth));

            var result = svc.GetConnectionStatus().GetAwaiter().GetResult();

            Con.WriteLine($"Success: {result.Success}");
            Con.WriteLine(new string('-', Con.WindowWidth));

            if (result.Success)
            {
                Con.WriteLine("Hardware");
                Con.WriteLine(new string('-', Con.WindowWidth - 1));
                Con.WriteLine($"Hardware Version: {result.HardwareVersion}");
                Con.WriteLine($"Software Version: {result.SoftwareVersion}");
                Con.WriteLine();

                Con.WriteLine("DSL Status");
                Con.WriteLine(new string('-', Con.WindowWidth - 1));
                Con.WriteLine();

                Con.WriteLine($"Status: {result.DslStatus}");
                Con.WriteLine($"Description: {result.DslDescription}");
                Con.WriteLine($"Current Rate: Up: {result.CurrentRate.Up} kbps   Down: {result.CurrentRate.Down} kbps");
                Con.WriteLine($"Maximum Rate: Up: {result.MaxRate.Up} kbps   Down: {result.MaxRate.Down} kbps");
                Con.WriteLine($"SNR: Up: {result.SnrMargin.Up} db   Down: {result.SnrMargin.Down} db");
                Con.WriteLine($"Line Attenuation: Up: {result.LineAttenuation.Up} db   Down: {result.LineAttenuation.Down} db");
                Con.WriteLine($"Error Packets: Up: {result.ErrorPackets.Up}   Down: {result.ErrorPackets.Down}");
                Con.WriteLine();

                Con.WriteLine("Internet Status");
                Con.WriteLine(new string('-', Con.WindowWidth - 1));
                Con.WriteLine();

                Con.WriteLine($"Status: {result.NetStatus}");
                Con.WriteLine($"External IP Address: {result.IpAddress}");
            }
            else
            {
                Con.WriteLine($"Error: {result.Error}");
            }
            Con.ReadLine();
        }
    }
}

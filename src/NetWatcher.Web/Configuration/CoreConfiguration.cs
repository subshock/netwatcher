﻿using Microsoft.Extensions.Configuration;
using NetWatcher.Core.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Configuration
{
    public class CoreConfiguration : ConfigurationBase
    {
        protected override string Section => "Core";

        public CoreConfiguration(IConfiguration config) : base(config)
        {
        }

        public string DbConnectionString { get; set; }
    }
}

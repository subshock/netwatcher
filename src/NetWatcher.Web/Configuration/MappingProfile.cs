﻿using AutoMapper;
using NetWatcher.Core.Models;
using NetWatcher.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Configuration
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ConnectionStatusInfo, StatusInfo>()
                .ForMember(a => a.CurrentRateDown, m => m.MapFrom(p => p.CurrentRate.Down))
                .ForMember(a => a.CurrentRateUp, m => m.MapFrom(p => p.CurrentRate.Up))
                .ForMember(a => a.DslUptimeSeconds, m => m.MapFrom(p => (int)p.DslUptime.TotalSeconds))
                .ForMember(a => a.Created, m => m.Ignore())
                .ForMember(a => a.ErrorPacketsDown, m => m.MapFrom(p => p.ErrorPackets.Down))
                .ForMember(a => a.ErrorPacketsUp, m => m.MapFrom(p => p.ErrorPackets.Up))
                .ForMember(a => a.Id, m => m.Ignore())
                .ForMember(a => a.LineAttenuationDown, m => m.MapFrom(p => p.LineAttenuation.Down))
                .ForMember(a => a.LineAttenuationUp, m => m.MapFrom(p => p.LineAttenuation.Up))
                .ForMember(a => a.MaxRateDown, m => m.MapFrom(p => p.MaxRate.Down))
                .ForMember(a => a.MaxRateUp, m => m.MapFrom(p => p.MaxRate.Up))
                .ForMember(a => a.SnrMarginDown, m => m.MapFrom(p => p.SnrMargin.Down))
                .ForMember(a => a.SnrMarginUp, m => m.MapFrom(p => p.SnrMargin.Up))
                .ForMember(a => a.TimeSpentMs, m => m.Ignore());
        }
    }
}

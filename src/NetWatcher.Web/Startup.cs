using System;
using System.Data;
using System.Data.SqlClient;
using AutoMapper;
using Dapper.GraphQL;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NetWatcher.Core.Configuration;
using NetWatcher.Core.Services;
using NetWatcher.Web.Configuration;
using NetWatcher.Web.Data;
using NetWatcher.Web.Services;

namespace NetWatcher.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(typeof(ServiceHttpClient<>));
            services.AddSingleton<CoreConfiguration>();
            services.AddSingleton<TplinkConfiguration>();
            services.AddScoped<IConnectionStatusProvider, TplinkConnectionStatus>();
            services.AddScoped<IStatusDataRepository, StatusDataRepository>();
            services.AddScoped<StatusService>();
            services.AddTransient<IDbConnection>(options =>
            {
                var config = options.GetRequiredService<CoreConfiguration>();

                return new SqlConnection(config.DbConnectionString);
            });

            services.AddDapperGraphQL(options =>
            {
                options.AddType<Data.Query.OrderByType>();
                options.AddType<GraphQL.Types.EnumerationGraphType<Core.Models.DslStatusType>>();
                options.AddType<GraphQL.Types.EnumerationGraphType<Core.Models.NetStatusType>>();
                options.AddType<Data.Query.StatusInfoType>();
                options.AddType<Data.Query.StatusInfoQuery>();

                options.AddSchema<Data.Query.StatusInfoSchema>();

                options.AddQueryBuilder<Models.StatusInfo, Data.Query.StatusInfoQueryBuilder>();

                options.AddEntityMapper<Models.StatusInfo, Data.Query.StatusInfoEntityMapper>();
            });

            // Set up hangfire
            services.AddHangfire(config =>
            {
                config.UseSqlServerStorage(Configuration.GetSection("Core")["DbConnectionString"]);
            });

            services.AddMvc();

            services.AddAutoMapper();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, IMapper mapper)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                Mapper.AssertConfigurationIsValid();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            // Set up hangfire
            app.UseHangfireServer();
            app.UseHangfireDashboard();
            RecurringJob.AddOrUpdate<StatusService>("GetStatusInfo", x => x.GetAndStoreStatus(), Cron.Minutely);

            app.UseGraphQL();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}

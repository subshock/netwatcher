﻿using NetWatcher.Core.Attributes;
using NetWatcher.Core.Models;
using System;

namespace NetWatcher.Web.Models
{
    public class StatusInfo
    {
        [GraphQLMap("id")]
        public Guid Id { get; set; }
        [GraphQLMap("created")]
        public DateTime Created { get; set; }
        [GraphQLMap("timeSpent")]
        public int TimeSpentMs { get; set; }

        [GraphQLMap("success")]
        public bool Success { get; set; }
        [GraphQLMap("error")]
        public string Error { get; set; }

        // Router Information
        [GraphQLMap("hwVersion")]
        public string HardwareVersion { get; set; }
        [GraphQLMap("swVersion")]
        public string SoftwareVersion { get; set; }

        // DSL Status
        [GraphQLMap("dslStatus")]
        public DslStatusType DslStatus { get; set; }
        [GraphQLMap("dslDescription")]
        public string DslDescription { get; set; }
        [GraphQLMap("dslUptime")]
        public int DslUptimeSeconds { get; set; }
        [GraphQLMap("currentRateUp")]
        public int CurrentRateUp { get; set; }
        [GraphQLMap("currentRateDown")]
        public int CurrentRateDown { get; set; }
        [GraphQLMap("maxRateUp")]
        public int MaxRateUp { get; set; }
        [GraphQLMap("maxRateDown")]
        public int MaxRateDown { get; set; }
        [GraphQLMap("snrMarginUp")]
        public decimal SnrMarginUp { get; set; }
        [GraphQLMap("snrMarginDown")]
        public decimal SnrMarginDown { get; set; }
        [GraphQLMap("lineAttenuationUp")]
        public decimal LineAttenuationUp { get; set; }
        [GraphQLMap("lineAttenuationDown")]
        public decimal LineAttenuationDown { get; set; }
        [GraphQLMap("errorPacketsUp")]
        public int ErrorPacketsUp { get; set; }
        [GraphQLMap("errorPacketsDown")]
        public int ErrorPacketsDown { get; set; }

        // Internet Status
        [GraphQLMap("netStatus")]
        public NetStatusType NetStatus { get; set; }
        [GraphQLMap("ipAddress")]
        public string IpAddress { get; set; }
    }
}

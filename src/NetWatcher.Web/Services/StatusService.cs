﻿using AutoMapper;
using NetWatcher.Core.Models;
using NetWatcher.Core.Services;
using NetWatcher.Web.Data;
using NetWatcher.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Services
{
    public class StatusService
    {
        private IConnectionStatusProvider _statusProvider;
        private IStatusDataRepository _statusRepo;
        private IMapper _mapper;

        public StatusService(IConnectionStatusProvider statusProvider, IStatusDataRepository statusRepo, IMapper mapper)
        {
            _statusProvider = statusProvider;
            _statusRepo = statusRepo;
            _mapper = mapper;
        }

        public async Task<bool> GetAndStoreStatus()
        {
            var sw = new Stopwatch();
            var timestamp = DateTime.UtcNow;
            ConnectionStatusInfo state = null;

            sw.Start();
            try
            {
                state = await _statusProvider.GetConnectionStatus();
                sw.Stop();
            }
            catch (Exception ex)
            {
                state = new ConnectionStatusInfo
                {
                    Success = false,
                    Error = ex.Message
                };
            }
            finally
            {
                sw.Stop();
            }

            try
            {
                var item = _mapper.Map<StatusInfo>(state);
                item.Created = timestamp;
                item.TimeSpentMs = Convert.ToInt32(sw.ElapsedMilliseconds);

                await _statusRepo.AddAsync(item);
            }
            catch (Exception ex)
            {
                state = new ConnectionStatusInfo
                {
                    Success = false,
                    Error = ex.Message
                };
            }

            return state.Success;
        }
    }
}

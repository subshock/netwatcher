﻿using GraphQL;
using GraphQL.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using NetWatcher.Web.Data.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Services
{
    public class GraphQLMiddleware
    {
        private RequestDelegate _next;
        private IServiceProvider _serviceProvider;

        public GraphQLMiddleware(RequestDelegate next, IServiceProvider serviceProvider)
        {
            _next = next;
            _serviceProvider = serviceProvider;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var sent = false;

            if (httpContext.Request.Path.StartsWithSegments("/graph"))
            {
                using (var sr = new StreamReader(httpContext.Request.Body))
                {
                    var query = await sr.ReadToEndAsync();

                    if (!string.IsNullOrWhiteSpace(query))
                    {
                        var schema = _serviceProvider.GetRequiredService<StatusInfoSchema>();
                        var result = await new DocumentExecuter()
                            .ExecuteAsync(options =>
                            {
                                options.Schema = schema;
                                options.Query = query;
                            }).ConfigureAwait(false);
                        CheckForErrors(result);

                        await WriteResult(httpContext, result);
                        sent = true;
                    }
                }
            }

            if (!sent)
                await _next(httpContext);
        }

        private async Task WriteResult(HttpContext httpContext, ExecutionResult result)
        {
            var json = new DocumentWriter(indent: true).Write(result);
            httpContext.Response.StatusCode = 200;
            httpContext.Response.ContentType = "application/json";
            await httpContext.Response.WriteAsync(json);
        }

        private void CheckForErrors(ExecutionResult result)
        {
            if (result.Errors?.Count > 0)
            {
                var errors = new List<Exception>();

                foreach (var error in result.Errors)
                {
                    var ex = error.InnerException != null ? new Exception(error.Message, error.InnerException) : new Exception(error.Message);

                    errors.Add(ex);
                }

                throw new AggregateException(errors);
            }
        }
    }

    public static class GraphQLMiddlewareExtensions
    {
        public static IApplicationBuilder UseGraphQL(this IApplicationBuilder builder) => builder.UseMiddleware<GraphQLMiddleware>();
    }
}

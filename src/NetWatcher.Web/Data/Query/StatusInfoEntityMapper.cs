﻿using Dapper.GraphQL;
using GraphQL.Language.AST;
using NetWatcher.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Data.Query
{
    public class StatusInfoEntityMapper : EntityMapper<StatusInfo>
    {
        public override StatusInfo Map(object[] objs, IHaveSelectionSet selectionSet = null, List<Type> splitOn = null)
        {
            StatusInfo status = null;

            foreach (var obj in objs)
            {
                if (status == null && obj is StatusInfo p)
                {
                    status = ResolveEntity(p);
                    continue;
                }
            }

            return status;
        }
    }
}

﻿using Dapper.GraphQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Data.Query
{
    public class CustomSqlQueryContext<TEntityType> : CustomSqlQueryContext
    {
        public CustomSqlQueryContext (string alias = null) 
            : base(alias == null ? typeof(TEntityType).Name : $"{typeof(TEntityType).Name} {alias}")
        {
            _types.Add(typeof(TEntityType));
        }
    }

    public class CustomSqlQueryContext : SqlQueryContext
    {
        private bool _pagination = false;
        private int _pageIndex;
        private int _pageSize;

        public CustomSqlQueryContext(string from)
            : base(from, null)
        {
        }

        public SqlQueryContext AddPagination(int pageIndex, int pageSize)
        {
            _pagination = true;
            _pageIndex = pageIndex;
            _pageSize = pageSize;

            return this;
        }

        public override string ToString()
        {
            var sql = base.ToString();

            if (_pagination)
            {
                sql += $" OFFSET {_pageIndex * _pageSize} ROWS FETCH NEXT {_pageSize} ROWS ONLY";
            }

            return sql;
        }
    }
}

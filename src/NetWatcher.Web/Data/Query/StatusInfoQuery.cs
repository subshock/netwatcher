﻿using Dapper.GraphQL;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Data.Query
{
    public class StatusInfoQuery : ObjectGraphType
    {
        public StatusInfoQuery(IEntityMapperFactory entityMapperFactory, IQueryBuilder<Models.StatusInfo> queryBuilder, IServiceProvider serviceProvider)
        {
            var defaultArgs = new QueryArguments(new QueryArgument<ListGraphType<OrderByType>> { Name = "orderby", Description = "An array of order statements" },
                    new QueryArgument<IntGraphType> { Name = "pageIndex", Description = "Pagignation index to retrieve", DefaultValue = 0 },
                    new QueryArgument<IntGraphType> { Name = "pageSize", Description = "Pagination size", DefaultValue = 50 },
                    new QueryArgument<BooleanGraphType> { Name = "success", Description = "Indicates whether the record successfully retrieved data from the device" }
                    );

            var typeInfo = serviceProvider.GetRequiredService<StatusInfoType>();

            foreach (var field in typeInfo.Fields)
                defaultArgs.Add(new QueryArgument(field.Type) { Name = field.Name, Description = field.Description });

            Field<ListGraphType<StatusInfoType>>("states",
                description: "A list of status info records",
                arguments: defaultArgs,
                resolve: context =>
                {
                    var alias = $"statusInfo_{Guid.NewGuid().ToString("N")}";
                    var query = new CustomSqlQueryContext($"StatusInfo {alias}");

                    queryBuilder.Build(query, context.FieldAst, alias);
                    int pageIndex = 0, pageSize = 50;
                    object[] orderBy = null;
                    var whereParamNum = 0;

                    foreach (var arg in context.Arguments)
                    {
                        switch (arg.Key.ToLowerInvariant())
                        {
                            case "pageindex":
                                pageIndex = (int)arg.Value;
                                break;
                            case "pagesize":
                                pageSize = (int)arg.Value;
                                break;
                            case "orderby":
                                orderBy = arg.Value as object[];
                                break;
                            default:
                                if (arg.Value != null)
                                {
                                    if (StatusInfoQueryBuilder.PropertyMapping.ContainsKey(arg.Key))
                                    {
                                        var wp = $"wp{whereParamNum++}";
                                        var p = new Dictionary<string, object>()
                                        {
                                            [wp] = arg.Value
                                        };
                                        query.AndWhere($"{alias}.{StatusInfoQueryBuilder.PropertyMapping[arg.Key]} = @{wp}", p);
                                    }
                                    else
                                        throw new ArgumentException($"{arg.Key} is not a valid field name");
                                }
                                break;
                        }
                    }

                    var mapper = entityMapperFactory.Build<Models.StatusInfo>(
                        state => state.Id,
                        context.FieldAst,
                        query.GetSplitOnTypes());

                    (pageIndex, pageSize) = ValidatePagination(pageIndex, pageSize);

                    // add order by parameters
                    orderBy = orderBy ?? new[] { OrderByType.DefaultOrderBy };
                    foreach (var orderByStmt in orderBy.Cast<OrderByType.OrderByModel>())
                        if (StatusInfoQueryBuilder.PropertyMapping.ContainsKey(orderByStmt.Field))
                            query.OrderBy($"{alias}.{StatusInfoQueryBuilder.PropertyMapping[orderByStmt.Field]} {orderByStmt.Direction:G}");
                        else
                            throw new ArgumentException($"{orderByStmt.Field} is not a valid field to order by");

                    query.AddPagination(pageIndex, pageSize);

                    using (var connection = serviceProvider.GetRequiredService<IDbConnection>())
                    {
                        connection.Open();

                        var results = query.Execute(connection, mapper);

                        return results;
                    }
                });

            FieldAsync<ListGraphType<StatusInfoType>>("statesAsync",
                description: "A list of status info records fetched asynchronously",
                resolve: async context =>
                {
                    var alias = $"statusInfo_{Guid.NewGuid().ToString("N")}";
                    var query = SqlBuilder.From($"StatusInfo {alias}");
                    query = queryBuilder.Build(query, context.FieldAst, alias);

                    var mapper = entityMapperFactory.Build<Models.StatusInfo>(
                        state => state.Id,
                        context.FieldAst,
                        query.GetSplitOnTypes());

                    using (var connection = serviceProvider.GetRequiredService<IDbConnection>())
                    {
                        connection.Open();

                        var results = await query.ExecuteAsync(connection, mapper);

                        return results;
                    }
                });

            Field<StatusInfoType>("status",
                description: "Gets a status info record by ID",
                arguments: new QueryArguments(
                    new QueryArgument<StringGraphType> { Name = "id", Description = "The ID of the status info record" }
                    ),
                resolve: context =>
                {
                    var id = context.Arguments["id"];
                    var alias = $"statusInfo_{Guid.NewGuid():N}";
                    var query = SqlBuilder
                        .From($"StatusInfo {alias}")
                        .Where($"{alias}.Id = @id", new { id });
                    query = queryBuilder.Build(query, context.FieldAst, alias);

                    var mapper = entityMapperFactory.Build<Models.StatusInfo>(
                        state => state.Id,
                        context.FieldAst,
                        query.GetSplitOnTypes());

                    using (var connection = serviceProvider.GetRequiredService<IDbConnection>())
                    {
                        connection.Open();

                        var results = query.Execute(connection, mapper);

                        return results.FirstOrDefault();
                    }
                });
        }

        private (int, int) ValidatePagination(int pageIndex, int pageSize)
        {
            if (pageIndex < 0) pageIndex = 0;
            if (pageSize <= 0 || pageSize > 100) pageIndex = 50;

            return (pageIndex, pageSize);
        }
    }
}

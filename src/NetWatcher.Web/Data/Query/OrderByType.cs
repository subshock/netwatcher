﻿using GraphQL.Language.AST;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Data.Query
{
    public class OrderByType : ScalarGraphType
    {
        public OrderByType()
        {
            Name = "OrderBy";
            Description = "An order by declaration";
        }

        public override object ParseLiteral(IValue value)
        {
            if (!(value is ObjectValue objVal)) return DefaultOrderBy;

            string field = null;
            var dir = OrderDirectionType.Asc;

            foreach (var result in objVal.ObjectFields)
            {
                switch (result.Name)
                {
                    case "field":
                        if (result.Value is StringValue fieldVal)
                            field = fieldVal.Value;
                        break;
                    case "dir":
                        if (result.Value is StringValue dirVal
                            && !Enum.TryParse(dirVal.Value, true, out dir))
                            throw new ArgumentException($"{dirVal.Value} is not a valid order direction");
                        break;
                    default:
                        throw new ArgumentException($"{result.Name} is not allowed as a field in the order by type");
                }
            }

            if (string.IsNullOrWhiteSpace(field))
                return DefaultOrderBy;

            return new OrderByModel
            {
                Field = field,
                Direction = dir
            };
        }

        public override object ParseValue(object value)
        {
            throw new NotImplementedException();
        }

        public override object Serialize(object value)
        {
            throw new NotImplementedException();
        }

        public static OrderByModel DefaultOrderBy = new OrderByModel { Field = "id", Direction = OrderDirectionType.Asc };

        public enum OrderDirectionType
        {
            Asc,
            Desc
        }

        public class OrderByModel
        {
            public string Field { get; set; }
            public OrderDirectionType Direction { get; set; }
        }
    }
}

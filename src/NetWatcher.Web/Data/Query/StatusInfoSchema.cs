﻿using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Data.Query
{
    public class StatusInfoSchema : Schema
    {
        public StatusInfoSchema(IServiceProvider serviceProvider, StatusInfoQuery query)
        {
            ResolveType = type => serviceProvider.GetRequiredService(type) as GraphType;
            Query = query;
        }
    }
}

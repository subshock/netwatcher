﻿using Dapper.GraphQL;
using GraphQL.Language.AST;
using NetWatcher.Core.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace NetWatcher.Web.Data.Query
{
    public class StatusInfoQueryBuilder : IQueryBuilder<Models.StatusInfo>
    {
        private static Lazy<Dictionary<string, string>> _propertyMapping;
        public static Dictionary<string, string> PropertyMapping => _propertyMapping.Value;

        static StatusInfoQueryBuilder()
        {
            _propertyMapping = new Lazy<Dictionary<string, string>>(GetPropertyMapping);
        }

        public SqlQueryContext Build(SqlQueryContext query, IHaveSelectionSet context, string alias)
        {
            query.Select($"{alias}.Id");
            query.SplitOn<Models.StatusInfo>("Id");

            var fields = context.GetSelectedFields();
            var map = PropertyMapping;

            foreach (var kvp in fields.Where(x => map.ContainsKey(x.Key) && x.Key != "id"))
                query.Select($"{alias}.{map[kvp.Key]}");

            return query;
        }

        private static Dictionary<string, string> GetPropertyMapping()
        {
            return typeof(Models.StatusInfo).GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Select(x => new
                {
                    x.Name,
                    Attr = x.GetCustomAttribute<GraphQLMapAttribute>()
                })
                .Where(x => x.Attr != null)
                .ToDictionary(x => x.Attr.GraphQLProperty, x => x.Name);
        }
    }
}

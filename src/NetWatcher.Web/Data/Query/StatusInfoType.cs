﻿using GraphQL.Types;
using NetWatcher.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Data.Query
{
    public class StatusInfoType : ObjectGraphType<Models.StatusInfo>
    {
        public StatusInfoType()
        {
            Name = "StatusInfo";
            Description = "A status info record";

            Field<StringGraphType>("id", description: "A unique identifier for the status record", resolve: context => context.Source?.Id);
            Field<DateGraphType>("created", description: "The date/time the record was created", resolve: context => context.Source?.Created);
            Field<BooleanGraphType>("success", description: "Determines if data was retrieved successfully", resolve: context => context.Source?.Success);
            Field<StringGraphType>("error", description: "Error message if unsuccessful", resolve: context => context.Source?.Error);
            Field<StringGraphType>("hwVersion", description: "The hardware version", resolve: context => context.Source?.HardwareVersion);
            Field<StringGraphType>("swVersion", description: "The software version", resolve: context => context.Source?.SoftwareVersion);
            Field<EnumerationGraphType<DslStatusType>>("dslStatus", description: "The status of the DSL connection", resolve: context => context.Source?.DslStatus);
            Field<StringGraphType>("dslDescription", description: "The description of the DSL connection", resolve: context => context.Source?.DslDescription);
            Field<IntGraphType>("dslUptime", description: "The uptime in seconds of the DSL connection", resolve: context => context.Source?.DslUptimeSeconds);
            Field<IntGraphType>("currentRateUp", description: "The current upstream rate in Kbps", resolve: context => context.Source?.CurrentRateUp);
            Field<IntGraphType>("currentRateDown", description: "The current downstream rate in Kbps", resolve: context => context.Source?.CurrentRateDown);
            Field<IntGraphType>("maxRateUp", description: "The estimated maximum upstream rate in Kbps", resolve: context => context.Source?.MaxRateUp);
            Field<IntGraphType>("maxRateDown", description: "The estimated maximum downstream rate in Kbps", resolve: context => context.Source?.MaxRateDown);
            Field<IntGraphType>("snrMarginUp", description: "The current upstream SNR in dB", resolve: context => context.Source?.SnrMarginUp);
            Field<IntGraphType>("snrMarginDown", description: "The current downstream SNR in dB", resolve: context => context.Source?.SnrMarginDown);
            Field<IntGraphType>("lineAttenuationUp", description: "The current upstream line attenuation in dB", resolve: context => context.Source?.LineAttenuationUp);
            Field<IntGraphType>("lineAttenuationDown", description: "The current downstream line attenuation in dB", resolve: context => context.Source?.LineAttenuationDown);
            Field<IntGraphType>("errorPacketsUp", description: "The cumulative number of upstream error packets", resolve: context => context.Source?.ErrorPacketsUp);
            Field<IntGraphType>("errorPacketsDown", description: "The cumulative number of downstream error packets", resolve: context => context.Source?.ErrorPacketsDown);
            Field<EnumerationGraphType<NetStatusType>>("netStatus", description: "The current status of the Internet connection", resolve: context => context.Source?.NetStatus);
            Field<StringGraphType>("ipAddress", description: "The currently assigned Internet IP address", resolve: context => context.Source?.IpAddress);
        }
    }
}

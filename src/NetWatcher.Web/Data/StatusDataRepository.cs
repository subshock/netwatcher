﻿using NetWatcher.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using NetWatcher.Web.Configuration;
using System.Data.SqlClient;

namespace NetWatcher.Web.Data
{
    public class StatusDataRepository : IStatusDataRepository, IDisposable
    {
        private CoreConfiguration _config;
        private IDbConnection _conn;

        protected IDbConnection Connection
        {
            get
            {
                if (_conn == null)
                    _conn = new SqlConnection(_config.DbConnectionString);

                if (_conn.State == ConnectionState.Closed || _conn.State == ConnectionState.Broken)
                    _conn.Open();

                return _conn;
            }
        }

        public StatusDataRepository(CoreConfiguration config)
        {
            _config = config;
        }

        public void Dispose()
        {
            _conn?.Dispose();
        }

        // todo: 
        // 4. build web backend
        // 7. build the frontend 
        // 8. Graphs:
        //  a. current up/down, max up/down
        //  b. snr, attenuation up/down
        //  c. dsl status, net status

        public async Task<Guid> AddAsync(StatusInfo item)
        {
            item.Id = Guid.NewGuid();

            await Connection.ExecuteAsync(@"INSERT INTO StatusInfo (Id, Created, TimeSpentMs, Success, Error, HardwareVersion, SoftwareVersion, DslStatus, DslDescription, DslUptimeSeconds,
    CurrentRateUp, CurrentRateDown, MaxRateUp, MaxRateDown, SnrMarginUp, SnrMarginDown, LineAttenuationUp, LineAttenuationDown, ErrorPacketsUp, ErrorPacketsDown, NetStatus, IpAddress)
    VALUES (@Id, @Created, @TimeSpentMs, @Success, @Error, @HardwareVersion, @SoftwareVersion, @DslStatus, @DslDescription, @DslUptimeSeconds, @CurrentRateUp, @CurrentRateDown, 
    @MaxRateUp, @MaxRateDown, @SnrMarginUp, @SnrMarginDown, @LineAttenuationUp, @LineAttenuationDown, @ErrorPacketsUp, @ErrorPacketsDown, @NetStatus, @IpAddress)", item);

            return item.Id;
        }

        public Guid Add(StatusInfo item)
        {
            return AddAsync(item).GetAwaiter().GetResult();
        }
    }
}

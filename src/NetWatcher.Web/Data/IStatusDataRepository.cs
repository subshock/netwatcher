﻿using NetWatcher.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetWatcher.Web.Data
{
    public interface IStatusDataRepository
    {
        Guid Add(StatusInfo item);
        Task<Guid> AddAsync(StatusInfo item);
    }
}
